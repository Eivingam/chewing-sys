extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=chewing");

    println!("cargo:rerun-if-changed=wrapper.h");

    let library = pkg_config::probe_library("chewing").unwrap();

    let pblist = library
        .include_paths[0]
        .clone()
        .into_os_string()
        .into_string()
        .unwrap();

    let bindings = bindgen::Builder::default()
        .clang_arg(format!("-I{}", &pblist))
        .header("wrapper.h")
        .blocklist_type("ChewingConfigData")
        .blocklist_function("bindgen_test_layout_ChewingConfigData")
        .blocklist_function("chewing_new2")
        .blocklist_function("chewing_KBStr2Num")
        .blocklist_function("chewing_get_KBString")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
