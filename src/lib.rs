#![allow(non_snake_case)]

include!("../bindings.rs");

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bindings() {
        unsafe {
            let ctx = chewing_new();
            let kbtype = chewing_get_KBType(ctx);
            println!("{}", kbtype);
            println!("Mode {}", chewing_get_ChiEngMode(ctx));
            chewing_set_ChiEngMode(ctx, 1);
            println!("mode {}", chewing_get_ChiEngMode(ctx));
            chewing_delete(ctx);
        }
    }
}
